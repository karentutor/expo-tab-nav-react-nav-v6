import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React, { useEffect } from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { getRandomRgb } from '../utils/getRandomRgb';

// returns screen and navigator
// provides for way to move between screens where each screen is placed on top of a stack
// will behave the same and perform natively

// each screen receives two parameters route and navigation
const Stack = createNativeStackNavigator();

const DetailScreen = ({ navigation, route }) => {
  const { randomColor } = route.params;

  const onUpdateColor = () => {
    const newColor = getRandomRgb();
    navigation.setOptions({
      headerTitle: newColor,
      headerStyle: {
        backgroundColor: newColor,
      },
    });
  };

  useEffect(() => {
    navigation.setOptions({
      headerStyle: {
        backgroundColor: randomColor,
      },
      headerTintColor: 'white',
      fontWeight: 'bold',

      headerRight: () => (
        <Button title="new color" color="black" onPress={onUpdateColor} />
      ),
    });
  }, []);

  return (
    <SafeAreaView
      style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
    >
      <Text>Detail Screen</Text>
      <Button
        title="Go Back"
        onPress={() => {
          navigation.goBack();
        }}
      />
      <Button
        title="Go Home"
        onPress={() => {
          navigation.navigate('Home');
        }}
      />
      <Button
        title="Go Home"
        onPress={() => {
          navigation.popToTop();
        }}
      />
      <Text>RGB received: {randomColor}</Text>
      <Button
        title="Update with received color"
        onPress={() => {
          navigation.setOptions({
            headerTitle: randomColor,
            title: randomColor,
            headerStyle: {
              backgroundColor: randomColor,
            },
          });
        }}
      />
    </SafeAreaView>
  );
};

const HomeScreen = ({ navigation }) => (
  <SafeAreaView
    style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
  >
    <Text>Home Screen</Text>
    <Button
      title="Go to Details"
      onPress={() => {
        navigation.navigate('Details', {
          randomColor: getRandomRgb(),
        });
      }}
    />
  </SafeAreaView>
);

// must have name and component
const NativeStackNavigatorExample = () => {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{ title: 'Overview ' }}
      />
      <Stack.Screen
        name="Details"
        component={DetailScreen}
        options={{ title: 'Overview ' }}
      />
    </Stack.Navigator>
  );
};

export default NativeStackNavigatorExample;
