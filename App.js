import { BottomTabBar } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';
import BottomTabNavigatorExample from './utils/BottomTabNavigator';

//entire must be inside SafeArea provider and view -- a separate package
// navigationContainer -- links to top level
export default function App() {
  return (
    <SafeAreaProvider>
      <NavigationContainer>
        {/* <NativeStackExample /> */}
        <BottomTabNavigatorExample />
      </NavigationContainer>
    </SafeAreaProvider>
  );
}
